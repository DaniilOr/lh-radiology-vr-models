# lh-vr-models

Machine learning models for VR.

## Getting started

This repository contains the Deep Learning models for classification, detection and segmentation of videos in VR environment.
All models are written in separate packages so that it is easy to use. 

Use cases. These models can:
- Classify the surgical tools on images (based on [this dataset](https://paperswithcode.com/dataset/cholec80))
- Segment polyps on surgical images (based on [this paper](https://arxiv.org/abs/2103.15715))
- Run multiclass segmentation of surgical images (based on [this data](https://arxiv.org/abs/2012.12453))
- Detect surgical tools (based on [this paper](https://eg.uc.pt/bitstream/10316/86257/1/Tese-Diana_VersaoFinal.pdf))

### Usage examples
All models have two files: `inference_func.py` and `inference_script.py`. 
`inference_func.py` contains a `predict` function, which is model specific and can be used in any part of your project
to apply the model to some data.

`inference_script.py` contains a script, which can be used to process a whole directory of images.

#### Segmentation
To apply the segmentaion model to a directory, you need to use the following command:
```bash
python inference_scripy.py --img-dir="DIRECTORY WITH IMAGES" --save-to="OUTPUT DIRECTORY" --onnx-model-path="ONNX FILE" --seg-type="SEGMENTATION TYPE"

```
It is important to mention that ``seg-type`` can be either `"binary"`, or `"multiclass"`.
#### Detection
The inference script for detection requires significantly less parameters. You simply need to run the following command from the *detection* directory
```bash
python inference_scripy.py --img-dir="DIRECTORY WITH IMAGES" --save-to="OUTPUT DIRECTORY"

```
#### Classification
This script generates a csv file, which contains the class labels for input images.
You can simply run the following command from the *classification* folder:
```bash
python inference_scripy.py --img-dir="DIRECTORY WITH IMAGES" --save-to="OUTPUT DIRECTORY"

```
#### Video processing
Some tools for video processing were added as well. You can find main tools in `video_annotator` folder.
Implementation of processing classes was inspired by this [repo](https://github.com/gorodion/pycv) of my collegue.

Examples on how to use video processing tools can be found in ``video_annotator/examples`` folder. It includes two simple pipelines
for applying the segmentation and detection models to a video.
