import os
import cv2
import argparse
import numpy as np
import pandas as pd
from typing import List, Union

from config import RESULT_NAME
from inference_func import predict_arr


def collect_images(dir: str):
    files = [os.path.join(dir, x) for x in os.listdir(dir)]
    imgs_array = [cv2.cvtColor(cv2.imread(file), cv2.COLOR_BGR2RGB) for file in files]
    return np.array(imgs_array)


def save_predictions(predictions: Union[np.ndarray, int],
                     names: List[str],
                     save_to: str):
    results_df = pd.DataFrame({"filename": names})
    results_df['predictions'] = pd.Series(map(lambda x:[x], predictions)).apply(lambda x:x[0])
    results_df.to_json(os.path.join(save_to, RESULT_NAME))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='run classification model from console.')
    parser.add_argument("--img-dir",  type=str, required=True,
                        help="path to the directory with images")
    parser.add_argument("--save-to", type=str, required=True,
                        help="prediction will be saved here")
    args = parser.parse_args()
    images = collect_images(args.img_dir)
    predictions = predict_arr(images)
    img_names = os.listdir(args.img_dir)
    save_predictions(predictions, img_names, args.save_to)