import onnxruntime as ort
import numpy as np

from config import *

sess = ort.InferenceSession(ONNX_MODEL, providers=["CUDAExecutionProvider"])


def predict(inp: np.ndarray):
    inp = inp.astype(np.float32)
    return sess.run([OUTPUT_NAME], {INPUT_NAME: inp})[0]