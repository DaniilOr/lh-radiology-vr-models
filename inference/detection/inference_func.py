import cv2
from PIL import Image
import numpy as np
from typing import Union
from inference.detection.model import model


def predict(img: Union[str, np.array],
            format: str='image'):
    if isinstance(img, str):
        prediction = model.predict(img)
    else:
        img = Image.fromarray(img)
        img.save('tmp.jpg')
        prediction = model.predict('tmp.jpg')
    return prediction.json()