from PIL import Image
from typing import Any

from inference.segmentation.dataset import *
from inference.segmentation.model import *
from inference.segmentation.config import *


def get_inference_augmentation():
    test_transform = [
        album.Resize(256, 256)
    ]
    return album.Compose(test_transform)


onnx_inference = get_onnx_model(MODEL_PATH)


def predict(img: np.ndarray,
            ds: InferenceDataset=InferenceEndoscopyDataset):
    """
        This function runs forward-pass of segmentation model on image
    :param img: input image
    :param ds: Inference Dataset instance for data processing
    :return: semgented image
    """
    img = ds.process_img(img)
    img = img[np.newaxis, :, :, :]
    ort_inputs = {onnx_inference.get_inputs()[0].name: img}
    ort_outs = onnx_inference.run(None, ort_inputs)
    return np.argmax(ort_outs[0][0, :, :, :], axis=0)


if __name__ == "__main__":
    p = predict('SAMPLE IMAGE', InferenceEndoscopyDataset)
    p = p[0, :, :, :]
    p = np.argmax(p, axis=0)
    img = Image.fromarray(p, "L")
    img.save('RESULTING IMAGE')