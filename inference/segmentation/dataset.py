import numpy as np

import torch
from typing import Union
import albumentations as album
import segmentation_models_pytorch as smp

import os
import cv2

from inference.segmentation.config import *


def to_tensor(x):
    return x.transpose(2, 0, 1).astype('float32')


def get_preprocessing(preprocessing_fn=None):
    _transform = []
    if preprocessing_fn:
        _transform.append(album.Lambda(image=preprocessing_fn))
    _transform.append(album.Lambda(image=to_tensor, mask=to_tensor))

    return album.Compose(_transform)


def get_inference_augmentation():
    test_transform = [
        album.Resize(256, 256)
    ]
    return album.Compose(test_transform)


class InferenceDataset(torch.utils.data.Dataset):
    """
        This class is used to prepare images for inference
    """
    augmentation = get_inference_augmentation()
    preprocessing = get_preprocessing(smp.encoders.get_preprocessing_fn(ENCODER, WEIGHTS))

    def __init__(self):
        self.image_paths = []

    def read_dir(self, dir: str):
        for dirname, _, filenames in os.walk(dir):
            for filename in filenames:
                p = os.path.join(dirname, filename)
                self.image_paths.append(p)

    @classmethod
    def process_img(cls, image: Union[np.ndarray, str]):
        """
            The image processing function can augment and preprocess image
        :param image: path to image or the image itself
        :return:
        """
        if isinstance(image, str):
            image = cv2.cvtColor(cv2.imread(image), cv2.COLOR_BGR2RGB)
        if cls.augmentation:
            sample = cls.augmentation(image=image)
            image = sample['image']

        if cls.preprocessing:
            sample = cls.preprocessing(image=image)
            image = sample['image']
        return image

    def __getitem__(self, i):
        img = InferenceDatasetCholec8k.process_img(self.image_paths[i])
        return img[np.newaxis, :, :, :], self.image_paths[i]

    def __len__(self):
        return len(self.image_paths)


class InferenceDatasetCholec8k(InferenceDataset):
    def __init__(self):
        super().__init__()


class InferenceEndoscopyDataset(InferenceDataset):
    def __init__(self):
        super().__init__()