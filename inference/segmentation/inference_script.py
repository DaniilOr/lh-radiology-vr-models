import argparse
import os

import onnxruntime
import numpy as np
from PIL import Image
from pathlib import Path

from inference.segmentation.dataset import InferenceDatasetCholec8k, InferenceEndoscopyDataset


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='run segmentation model from console.')
    parser.add_argument("--img-dir",  type=str, required=True,
                        help="path to the directory with images")
    parser.add_argument("--save-to", type=str, required=True,
                        help="masks will be saved here")
    parser.add_argument("--onnx-model-path", type=str, required=True,
                        help="path to onnx model")
    parser.add_argument("--seg-type", type=str, required=True,
                        help="binary or multiclass segmentation", choices=["binary", "multiclass"])
    args = parser.parse_args()

    ort_session = onnxruntime.InferenceSession(args.onnx_model_path)
    if args.seg_type == "multiclass":
        data = InferenceDatasetCholec8k()
    elif args.seg_type == "binary":
        data = InferenceEndoscopyDataset()
    else:
        raise NotImplemented
    data.read_dir(args.img_dir)
    save_to = Path(args.save_to)
    if not os.path.exists(save_to):
        os.mkdir(save_to)

    assert os.path.isdir(save_to), "invalid directory name"

    for img, name in data:
        name = name.split('/')[-1]
        ort_inputs = {ort_session.get_inputs()[0].name: img}
        ort_outs = ort_session.run(None, ort_inputs)[0]
        ort_outs = np.argmax(ort_outs[0, :, :, :], axis=0)
        img = Image.fromarray(ort_outs, "L")
        img.save(save_to / name)
