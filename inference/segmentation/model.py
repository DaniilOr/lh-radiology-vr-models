from typing import Optional

import segmentation_models_pytorch as smp
import torch
import torch.onnx
import onnxruntime


def to_numpy(tensor):
    return tensor.detach().cpu().numpy() if tensor.requires_grad else tensor.cpu().numpy()


def get_onnx_model(model_path: str):
    ort_session = onnxruntime.InferenceSession(model_path)
    return ort_session


def get_torch_model(encoder: str,
                    encoder_weights: str,
                    n_classes: int,
                    use_pretrained: bool=True,
                    pretrained_path: Optional[str]=None,
                    activation: Optional[str]='softmax2d'):
    model = smp.DeepLabV3Plus(
        encoder_name=encoder,
        encoder_weights=encoder_weights,
        classes=n_classes,
        activation=activation,
    )
    map_location = lambda storage, loc: storage
    if torch.cuda.is_available():
        map_location = None
    if use_pretrained:
        assert pretrained_path is not None, 'No pretrained weights'
        model.load_state_dict(torch.load(pretrained_path,  map_location=map_location))
    return model
