from scipy import ndimage
import numpy as np


def preprocess(im: np.ndarray,
               k_1: float = 0.25,
               k_2: float = 0.25) -> np.array:
    """
    This function simulates VR adding barrel distortion to images
    :param im: image
    :param k_1: distortion coefficient
    :param k_2: distortion coefficient
    :return: destorted image
    """
    h, w, _ = im.shape

    x, y = np.meshgrid(np.float32(np.arange(w)),
                       np.float32(np.arange(h)))
    x_c = w/2
    y_c = h/2
    x = x - x_c
    y = y - y_c
    x = x/x_c
    y = y/y_c

    radius = np.sqrt(x ** 2 + y ** 2)

    m_r = 1 + k_1 * radius + k_2 * radius ** 2

    x = x * m_r
    y = y * m_r

    x = x * x_c + x_c
    y = y * y_c + y_c

    distorted = np.dstack([ndimage.map_coordinates(im[:, :, i],
                                                   [y.ravel(), x.ravel()]) for i in range(3)])
    distorted.resize(im.shape)
    return distorted
