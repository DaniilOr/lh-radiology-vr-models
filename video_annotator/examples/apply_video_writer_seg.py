"""
This program shows how to run ProcessedWriter from my VideoWriter file and combine it with a segmentation model
    When you run this program, a new video will be saved to the specified file (after application of transformations)
    I recommend using one of my pretrained models (since their predict function is available)
"""

from prediction_utils import segmentation_inference, preprocess, ProcessedWriter, CMAP, FOURCC


if __name__ == "__main__":

    writer = ProcessedWriter("PATH TO SOURCE VIDEO",
                             "OUTPUT PATH",
                             30,
                             FOURCC,
                             CMAP,
                             preprocess, segmentation_inference.predict)
    writer.write_file()
