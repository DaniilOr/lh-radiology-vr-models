"""
    This program shows how to run VideoProcessor with a segmentation model
    When you run this program, a new video will be displayed as a clip
    I recommend using one of my pretrained models (since their predict function is available)
"""
import cv2, numpy as np
from prediction_utils import segmentation_inference, preprocess, VideoProcessor, CMAP


if __name__ == "__main__":

    with VideoProcessor('PATH TO SOURCE VIDEO',
                        preprocess, segmentation_inference.predict) as v:
        for i in v:
            img = i.astype(np.uint8)
            img = CMAP[img, :]
            cv2.imshow('frame',  img.astype(np.uint8))
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        cv2.destroyAllWindows()
