import cv2
from pathlib import Path
from decimal import Decimal
from cv2 import VideoCapture
from typing import Union, Optional, Callable, List


class VideoProcessor(VideoCapture):
    """
        This class creates an iterator for video processing.
        It is inherited from openCVs VideoCapture class, so
            the methods are similar
    """
    def __init__(
            self, src: Union[str, Path], *transforms: Optional[List[Callable]],
    ):
        """

        :param src: path to the video source or source id
        :param transforms: a list of transforms and/or prediction functions
        """
        # if the device id is given as a string
        if isinstance(src, str) and src.isdecimal():
            src = int(src)
        # if a filename is given
        elif isinstance(src, (str, Path)):
            if not Path(src).is_file():
                raise FileNotFoundError(str(src))
            src = str(src)

        super().__init__(src)
        assert self.isOpened(), f'Failed to open {src}'

        self.transforms = transforms

        self.frame_cnt = int(self.get(cv2.CAP_PROP_FRAME_COUNT))
        self.fps = int(self.get(cv2.CAP_PROP_FPS))
        self.width = int(self.get(cv2.CAP_PROP_FRAME_WIDTH))
        self.height = int(self.get(cv2.CAP_PROP_FRAME_HEIGHT))
        self.shape = (self.width, self.height)

    @property
    def now(self):
        return int(self.get(cv2.CAP_PROP_POS_FRAMES))

    def get_frame(self):

        assert self.isOpened(), f'Video is not opened'

        _, frame = super().read()

        if frame is None:
            raise StopIteration('Video has finished')

        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        # apply transforms to the frame
        if self.transforms:
            for t in self.transforms:
                frame = t(frame)
        return frame

    def __iter__(self):
        return self

    def __next__(self):
        frame = self.get_frame()
        return frame

    def rewind(self, nframe):
        assert isinstance(nframe, Decimal), f'{nframe} is not an integer'
        assert nframe in range(
            0, len(self),
        ), f'{nframe} is not an appropriate frame number for a video of length {len(self)}'
        self.set(cv2.CAP_PROP_POS_FRAMES, nframe)
        return self

    def __len__(self):
        return self.frame_cnt

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.release()

    def close_file(self):
        self.release()
